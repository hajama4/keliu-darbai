<?php

namespace App\Entity;

use App\Repository\GeneralRoadSectionsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GeneralRoadSectionsRepository::class)
 */
class GeneralRoadSections
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $RoadName;

    /**
     * @ORM\Column(type="float")
     */
    private $SectionStart;

    /**
     * @ORM\Column(type="float")
     */
    private $SectionEnd;

    /**
     * @ORM\Column(type="integer")
     */
    private $RoadLevel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $RoadType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $RoadNumber;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoadName(): ?string
    {
        return $this->RoadName;
    }

    public function setRoadName(string $RoadName): self
    {
        $this->RoadName = $RoadName;

        return $this;
    }

    public function getSectionStart(): ?float
    {
        return $this->SectionStart;
    }

    public function setSectionStart(float $SectionStart): self
    {
        $this->SectionStart = $SectionStart;

        return $this;
    }

    public function getSectionEnd(): ?float
    {
        return $this->SectionEnd;
    }

    public function setSectionEnd(float $SectionEnd): self
    {
        $this->SectionEnd = $SectionEnd;

        return $this;
    }

    public function getRoadLevel(): ?int
    {
        return $this->RoadLevel;
    }

    public function setRoadLevel(int $RoadLevel): self
    {
        $this->RoadLevel = $RoadLevel;

        return $this;
    }

    public function getRoadType(): ?string
    {
        return $this->RoadType;
    }

    public function setRoadType(string $RoadType): self
    {
        $this->RoadType = $RoadType;

        return $this;
    }

    public function getRoadNumber(): ?string
    {
        return $this->RoadNumber;
    }

    public function setRoadNumber(string $RoadNumber): self
    {
        $this->RoadNumber = $RoadNumber;

        return $this;
    }
}
