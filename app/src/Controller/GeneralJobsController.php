<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Helper\FormJsonResponse;
use App\Helper\GeneralJobs\FormResults;

class GeneralJobsController extends AbstractController
{
    /**
     * @var FormJsonResponse
     */
    private $formJsonResponse;

    /**
     * @var FormResults
     */
    private $formResults;

    public function __construct
    (
        FormJsonResponse $formJsonResponse,
        FormResults $formResults
    )
    {
        $this->formJsonResponse = $formJsonResponse;
        $this->formResults = $formResults;
    }

    /**
     * @Route("/general-jobs", name="general_jobs", methods="GET")
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $results = $this->formResults->getAllResults();
        $respond = $this->formJsonResponse;

        return $respond->respond($results);
    }

    /**
     * @Route("/general-jobs", methods="POST")
     */
    public function create(Request $request)
    {

        if (!$request) {
            return $this->respondValidationError('Please provide a valid request!');
        }

        $data = json_decode($request->getContent($request), true);

        //TODO: create method to handle validation errors

        // validate the code
        if (!$data['code']) {
            return $this->respondValidationError('Please provide a code!');
        }

        // validate the name
        if (!$data['name']) {
            return $this->respondValidationError('Please provide a name!');
        }

        // validate the units
        if (!$data['lengthUnit']) {
            return $this->respondValidationError('Please provide a length of units!');
        }


        $record = $this->formResults->addNewRecord($data);

        //TODO Validate if no errors saving records in database

        $respond = $this->formJsonResponse;

        return $respond->respondCreated($record);
    }

    /**
     * @Route("/general-jobs/edit/{id}", methods="POST")
     */
    public function update(Request $request, $id)
    {

        if (!$request) {
            return $this->respondValidationError('Please provide a valid request!');
        }

        $data = json_decode($request->getContent($request), true);

        //TODO: create method to handle validation errors

        // validate the code
        if (!$data['code']) {
            return $this->respondValidationError('Please provide a code!');
        }

        // validate the name
        if (!$data['name']) {
            return $this->respondValidationError('Please provide a name!');
        }

        // validate the units
        if (!$data['lengthUnit']) {
            return $this->respondValidationError('Please provide a length of units!');
        }


        $record = $this->formResults->updateRecord($data, $id);

        //TODO Validate if no errors saving records in database

        $respond = $this->formJsonResponse;

        return $respond->respondCreated($record);
    }
}