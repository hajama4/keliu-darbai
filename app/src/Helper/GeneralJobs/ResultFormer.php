<?php


namespace App\Helper\GeneralJobs;


use App\Entity\GeneralJobs;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class ResultFormer
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var App\Repository\GeneralJobsRepository
     */
    private $generalJobsRepository;


    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->generalJobsRepository = $em->getRepository("App:GeneralJobs");
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return $this->generalJobsRepository->findAll();
    }

    /**
     * @param array $generalJobs
     * @return array
     */
    public function form($generalJobs)
    {
        $generalJobsArray = [];

        foreach($generalJobs as $generalJob) {
            $generalJobsArray[] = [
                'id'    => (int) $generalJob->getId(),
                'code' => (string) $generalJob->getCode(),
                'name' => (string) $generalJob->getName(),
                'lengthUnits' => (string) $generalJob->getLenghtUnits()
            ];
        }

        return $generalJobsArray;
    }

    /**
     * @param array $generalJob
     * @return array
     */
    public function formOne($generalJob)
    {
        $generalJob = [
            'id'    => (int) $generalJob->getId(),
            'code' => (string) $generalJob->getCode(),
            'name' => (string) $generalJob->getName(),
            'lengthUnits' => (string) $generalJob->getLenghtUnits()
        ];

        return $generalJob;
    }

    /**
     * @param array $record
     * @return GeneralJobs
     */
    public function addRecord($record)
    {
        $generalJob = new GeneralJobs();
        $generalJob->setCode($record['code']);
        $generalJob->setName($record['name']);
        $generalJob->setLenghtUnits($record['lengthUnit']);
        $this->em->persist($generalJob);
        $this->em->flush();

        return $generalJob;
        //TODO: return errors
    }

    /**
     * @param array $record
     * @param int $id
     * @return object|null
     */
    public function updateRecord($record, $id)
    {
        $generalJob = $this->generalJobsRepository->find($id);
        $generalJob->setCode($record['code']);
        $generalJob->setName($record['name']);
        $generalJob->setLenghtUnits($record['lengthUnit']);
        $this->em->persist($generalJob);
        $this->em->flush();

        return $generalJob;
        //TODO: return errors
    }
}