<?php


namespace App\Helper\GeneralJobs;

use App\Helper\GeneralJobs\ResultFormer;

class FormResults
{
    /**
     * @var \App\Helper\GeneralJobs\ResultFormer
     */
    private $resultFormer;

    public function __construct(ResultFormer $resultFormer)
    {
        $this->resultFormer = $resultFormer;
    }

    /**
     * @return array
     */
    public function getAllResults()
    {
        $generalJobs = $this->resultFormer->getAll();
        $generalJobs = $this->resultFormer->form($generalJobs);

        return $generalJobs;
    }

    /**
     * @param array $generalJob
     * @return array
     */
    public function addNewRecord($generalJob)
    {
        $record = $this->resultFormer->addRecord($generalJob);

        return $this->resultFormer->formOne($record);//TODO: return error
    }

    /**
     * @param array $generalJob
     * @param int $id
     * @return array
     */
    public function updateRecord($generalJob, $id)
    {
        $record = $this->resultFormer->updateRecord($generalJob, $id);

        return $this->resultFormer->formOne($record);
    }
}