<?php

namespace App\Repository;

use App\Entity\GeneralRoadSections;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GeneralRoadSections|null find($id, $lockMode = null, $lockVersion = null)
 * @method GeneralRoadSections|null findOneBy(array $criteria, array $orderBy = null)
 * @method GeneralRoadSections[]    findAll()
 * @method GeneralRoadSections[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GeneralRoadSectionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GeneralRoadSections::class);
    }

    // /**
    //  * @return GeneralRoadSections[] Returns an array of GeneralRoadSections objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GeneralRoadSections
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
