import Vue from 'vue';
import VueRouter from 'vue-router';
import BootstrapVue from 'bootstrap-vue';
global.jQuery = require('jquery');
global.axios = require('axios');
import '../css/app.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import vmodal from 'vue-js-modal';

Vue.use(vmodal);
Vue.use(VueRouter);
Vue.use(BootstrapVue);

import App from './components/App';
import GeneralJobs from './components/GeneralJobs';

const routes = [
    { path: '/', component: App },
    { path: '/general-jobs', component: GeneralJobs},
    { path: "*", redirect: "/" }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

new Vue({
    router: router,
    el: '#app',
    render: h => h(App)
});